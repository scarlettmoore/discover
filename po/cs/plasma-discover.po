# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2023 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-10 01:47+0000\n"
"PO-Revision-Date: 2023-11-07 15:35+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"

#: discover/DiscoverObject.cpp:161
#, kde-format
msgctxt "@title %1 is the distro name"
msgid ""
"%1 is not configured for installing apps through Discover—only app add-ons"
msgstr ""

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the distro name"
msgid ""
"To use Discover for apps, install your preferred module on the "
"<interface>Settings</interface> page, under <interface>Missing Backends</"
"interface>."
msgstr ""

#: discover/DiscoverObject.cpp:166
#, kde-format
msgctxt "@action:button %1 is the distro name"
msgid "Report This Issue to %1"
msgstr "Nahlaste tento problém na %1"

#: discover/DiscoverObject.cpp:171
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is the distro name; in this case it always contains 'Arch "
"Linux'"
msgid ""
"To use Discover for apps, install <link url='https://wiki.archlinux.org/"
"title/Flatpak#Installation'>Flatpak</link> or <link url='https://wiki."
"archlinux.org/title/KDE#Discover_does_not_show_any_applications'>PackageKit</"
"link> using the <command>pacman</command> package manager.<nl/><nl/> Review "
"<link url='https://archlinux.org/packages/extra/x86_64/discover/'>%1's "
"packaging for Discover</link>"
msgstr ""

#: discover/DiscoverObject.cpp:266
#, kde-format
msgid "Could not find category '%1'"
msgstr "Nelze nalézt kategorii '%1'"

#: discover/DiscoverObject.cpp:281
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr "Dialog pro otevření neexistujícího souboru '%1'"

#: discover/DiscoverObject.cpp:303
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""

#: discover/DiscoverObject.cpp:307
#, kde-format
msgid "Could not open %1"
msgstr "Nelze otevřít %1"

#: discover/DiscoverObject.cpp:369
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "Prosím zkontrolujte, zda je Snap správně nainstalován."

#: discover/DiscoverObject.cpp:371
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr "Prosím nahlaste tuto chybu správcům vaší distribuce."

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Report This Issue"
msgstr "Nahlásit tuto chybu"

#: discover/DiscoverObject.cpp:439 discover/DiscoverObject.cpp:441
#: discover/main.cpp:121
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:442
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""

#: discover/main.cpp:42
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr ""

#: discover/main.cpp:43
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr ""

#: discover/main.cpp:44
#, kde-format
msgid "Display a list of entries with a category."
msgstr ""

#: discover/main.cpp:45
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""

#: discover/main.cpp:46
#, kde-format
msgid "List all the available modes."
msgstr "Vypsat všechny dostupné režimy."

#: discover/main.cpp:47
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr ""

#: discover/main.cpp:48
#, kde-format
msgid "Local package file to install"
msgstr "Lokální soubor s balíčkem k instalaci"

#: discover/main.cpp:49
#, kde-format
msgid "List all the available backends."
msgstr "Vypsat všechny dostupné podpůrné vrstvy."

#: discover/main.cpp:50
#, kde-format
msgid "Search string."
msgstr "Řetězec pro hledání."

#: discover/main.cpp:51
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Seznam dostupných voleb pro uživatelskou zpětnou vazbu"

#: discover/main.cpp:53
#, kde-format
msgid "Supports appstream: url scheme"
msgstr ""

#: discover/main.cpp:123
#, kde-format
msgid "An application explorer"
msgstr "Prohlížeč aplikací"

#: discover/main.cpp:125
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2022 Vývojový tým Plasma"

#: discover/main.cpp:126
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:127
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:128
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "Zajištění kvality, návrh a použitelnost"

#: discover/main.cpp:132
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:133
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:140
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#: discover/main.cpp:140
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: discover/main.cpp:153
#, kde-format
msgid "Available backends:\n"
msgstr "Dostupné podpůrné vrstvy:\n"

#: discover/main.cpp:209
#, kde-format
msgid "Available modes:\n"
msgstr "Dostupné režimy:\n"

#: discover/qml/AddonsView.qml:20 discover/qml/navigation.js:43
#, kde-format
msgid "Addons for %1"
msgstr "Doplňky pro %1"

#: discover/qml/AddonsView.qml:52
#, kde-format
msgid "More…"
msgstr "Více…"

#: discover/qml/AddonsView.qml:61
#, kde-format
msgid "Apply Changes"
msgstr "Provést změny"

#: discover/qml/AddonsView.qml:68
#, kde-format
msgid "Reset"
msgstr "Obnovit"

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr "Přidat nový repozitář %1"

#: discover/qml/AddSourceDialog.qml:44
#, kde-format
msgid "Add"
msgstr "Přidat"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:274
#: discover/qml/InstallApplicationButton.qml:45
#: discover/qml/ProgressView.qml:104 discover/qml/SourcesPage.qml:191
#: discover/qml/UpdatesPage.qml:259 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "Zrušit"

#: discover/qml/ApplicationDelegate.qml:185
#: discover/qml/ApplicationPage.qml:221
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 hodnocení"
msgstr[1] "%1 hodnocení"
msgstr[2] "%1 hodnocení"

#: discover/qml/ApplicationDelegate.qml:185
#: discover/qml/ApplicationPage.qml:221
#, kde-format
msgid "No ratings yet"
msgstr "Zatím žádná hodnocení"

#: discover/qml/ApplicationPage.qml:72
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr "Z %1"

#: discover/qml/ApplicationPage.qml:83
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:200
#, kde-format
msgid "Unknown author"
msgstr "Neznámý autor"

#: discover/qml/ApplicationPage.qml:245
#, kde-format
msgid "Version:"
msgstr "Verze:"

#: discover/qml/ApplicationPage.qml:257
#, kde-format
msgid "Size:"
msgstr "Velikost:"

#: discover/qml/ApplicationPage.qml:269
#, kde-format
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "Licence:"
msgstr[1] "Licence:"
msgstr[2] "Licence:"

#: discover/qml/ApplicationPage.qml:277
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "Neznámý"

#: discover/qml/ApplicationPage.qml:307
#, kde-format
msgid "What does this mean?"
msgstr "Co to znamená?"

#: discover/qml/ApplicationPage.qml:316
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "Více…"
msgstr[1] "Více…"
msgstr[2] "Více…"

#: discover/qml/ApplicationPage.qml:327
#, kde-format
msgid "Content Rating:"
msgstr "Hodnocení obsahu:"

#: discover/qml/ApplicationPage.qml:336
#, kde-format
msgid "Age: %1+"
msgstr "Věk: %1+"

#: discover/qml/ApplicationPage.qml:356
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr "Podrobnosti…"

#: discover/qml/ApplicationPage.qml:378
#, kde-format
msgctxt "@info placeholder message"
msgid "Screenshots not available for %1"
msgstr "Pro %1 není dostupný snímek obrazovky"

#: discover/qml/ApplicationPage.qml:551
#, kde-format
msgid "Documentation"
msgstr "Dokumentace"

#: discover/qml/ApplicationPage.qml:552
#, kde-format
msgid "Read the project's official documentation"
msgstr "Přečtěte si oficiální dokumentaci projektu"

#: discover/qml/ApplicationPage.qml:568
#, kde-format
msgid "Website"
msgstr "Webová stránka"

#: discover/qml/ApplicationPage.qml:569
#, kde-format
msgid "Visit the project's website"
msgstr "Přejít na stránku projektu"

#: discover/qml/ApplicationPage.qml:585
#, kde-format
msgid "Addons"
msgstr "Doplňky"

#: discover/qml/ApplicationPage.qml:586
#, kde-format
msgid "Install or remove additional functionality"
msgstr "Instalace nebo odstranění dodatečné funkčnosti"

#: discover/qml/ApplicationPage.qml:605
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr "Sdílet"

#: discover/qml/ApplicationPage.qml:606
#, kde-format
msgid "Send a link for this application"
msgstr "Poslat odkaz pro tuto aplikaci"

#: discover/qml/ApplicationPage.qml:622
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr ""

#: discover/qml/ApplicationPage.qml:642
#, kde-format
msgid "What's New"
msgstr "Co je nového"

#: discover/qml/ApplicationPage.qml:672
#, kde-format
msgid "Reviews"
msgstr "Recenze"

#: discover/qml/ApplicationPage.qml:684
#, kde-format
msgid "Loading reviews for %1"
msgstr "Načítají se recenze pro %1"

#: discover/qml/ApplicationPage.qml:692
#, kde-format
msgctxt "@info placeholder message"
msgid "Reviews for %1 are temporarily unavailable"
msgstr "Hodnocení pro %1 jsou dočasně nedostupná"

#: discover/qml/ApplicationPage.qml:716
#, kde-format
msgctxt "@action:button"
msgid "Show All Reviews"
msgstr "Zobrazit všechny recenze"

#: discover/qml/ApplicationPage.qml:728
#, kde-format
msgid "Write a Review"
msgstr "Napište hodnocení..."

#: discover/qml/ApplicationPage.qml:728
#, kde-format
msgid "Install to Write a Review"
msgstr ""

#: discover/qml/ApplicationPage.qml:740
#, kde-format
msgid "Get Involved"
msgstr "Zapojte se"

#: discover/qml/ApplicationPage.qml:782
#, kde-format
msgid "Donate"
msgstr "Přispět"

#: discover/qml/ApplicationPage.qml:783
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""

#: discover/qml/ApplicationPage.qml:799
#, kde-format
msgid "Report Bug"
msgstr "Nahlásit chybu"

#: discover/qml/ApplicationPage.qml:800
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr ""

#: discover/qml/ApplicationPage.qml:816
#, kde-format
msgid "Contribute"
msgstr "Přispět"

#: discover/qml/ApplicationPage.qml:817
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr ""

#: discover/qml/ApplicationPage.qml:842
#, kde-format
msgid "All Licenses"
msgstr "Všechny licence"

#: discover/qml/ApplicationPage.qml:872
#, kde-format
msgid "Content Rating"
msgstr "Hodnocení obsahu"

#: discover/qml/ApplicationPage.qml:888
#, kde-format
msgid "Risks of proprietary software"
msgstr "Rizika proprietárního software"

#: discover/qml/ApplicationPage.qml:894
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""

#: discover/qml/ApplicationPage.qml:895
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:53
#, kde-format
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "Hledat: %2 - %3 položku"
msgstr[1] "Hledat: %2 - %3 položky"
msgstr[2] "Hledat: %2 - %3 položek"

#: discover/qml/ApplicationsListPage.qml:55
#, kde-format
msgid "Search: %1"
msgstr "Hledat: %1"

#: discover/qml/ApplicationsListPage.qml:59
#, kde-format
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%2 - %1 položka"
msgstr[1] "%2 - %1 položky"
msgstr[2] "%2 - %1 položek"

#: discover/qml/ApplicationsListPage.qml:65
#, kde-format
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "Hledat - %1 položku"
msgstr[1] "Hledat - %1 položky"
msgstr[2] "Hledat - %1 položek"

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:261
#, kde-format
msgid "Search"
msgstr "Hledat"

#: discover/qml/ApplicationsListPage.qml:98 discover/qml/ReviewsPage.qml:98
#, kde-format
msgid "Sort: %1"
msgstr "Seřadit: %1"

#: discover/qml/ApplicationsListPage.qml:103
#, kde-format
msgctxt "Search results most relevant to the search query"
msgid "Relevance"
msgstr "Relevance"

#: discover/qml/ApplicationsListPage.qml:114
#, kde-format
msgid "Name"
msgstr "Název"

#: discover/qml/ApplicationsListPage.qml:125 discover/qml/Rating.qml:117
#, kde-format
msgid "Rating"
msgstr "Hodnocení"

#: discover/qml/ApplicationsListPage.qml:136
#, kde-format
msgid "Size"
msgstr "Velikost"

#: discover/qml/ApplicationsListPage.qml:147
#, kde-format
msgid "Release Date"
msgstr "Datum vydání"

#: discover/qml/ApplicationsListPage.qml:207
#, kde-format
msgid "Nothing found"
msgstr "Nic nenalezeno"

#: discover/qml/ApplicationsListPage.qml:215
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr "Hledat ve všech kategoriích"

#: discover/qml/ApplicationsListPage.qml:225
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "Hledat \"%1\" na webu"

#: discover/qml/ApplicationsListPage.qml:229
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:240
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "\"%1\" nebylo nalezeno v kategorii \"%2\""

#: discover/qml/ApplicationsListPage.qml:242
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "\"%1\" nebylo v dostupných zdrojích nalezeno"

#: discover/qml/ApplicationsListPage.qml:243
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""

#: discover/qml/ApplicationsListPage.qml:276
#, kde-format
msgid "Still looking…"
msgstr "Stále hledám…"

#: discover/qml/BrowsingPage.qml:20
#, kde-format
msgctxt "@title:window the name of a top-level 'home' page"
msgid "Home"
msgstr "Domů"

#: discover/qml/BrowsingPage.qml:66
#, kde-format
msgid "Unable to load applications"
msgstr "Aplikace nelze načíst"

#: discover/qml/BrowsingPage.qml:104
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr "Nejoblíbenější"

#: discover/qml/BrowsingPage.qml:124
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr "Výběr editorů"

#: discover/qml/BrowsingPage.qml:141
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr "Nejlépe hodnocené hry"

#: discover/qml/BrowsingPage.qml:160 discover/qml/BrowsingPage.qml:189
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr "Více"

#: discover/qml/BrowsingPage.qml:170
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr "Nejlépe hodnocené Vývojové nástroje"

#: discover/qml/CarouselDelegate.qml:212
#, kde-format
msgctxt "@action:button Start playing media"
msgid "Play"
msgstr "Přehrát"

#: discover/qml/CarouselDelegate.qml:214
#, kde-format
msgctxt "@action:button Pause any media that is playing"
msgid "Pause"
msgstr "Pozastavit"

#: discover/qml/CarouselMaximizedViewContent.qml:40
#, kde-format
msgctxt "@action:button"
msgid "Switch to Overlay"
msgstr "Přepnout na rozložení"

#: discover/qml/CarouselMaximizedViewContent.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Switch to Full Screen"
msgstr "Přepnout do režimu na celou obrazovku"

#: discover/qml/CarouselMaximizedViewContent.qml:75
#, kde-format
msgctxt ""
"@action:button Close overlay/window/popup with carousel of screenshots"
msgid "Close"
msgstr "Zavřít"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Previous Screenshot"
msgstr "Předchozí snímek obrazovky"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Next Screenshot"
msgstr "Následující snímek obrazovky"

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr ""

#: discover/qml/DiscoverWindow.qml:56
#, kde-format
msgid "&Home"
msgstr "&Domů"

#: discover/qml/DiscoverWindow.qml:66
#, kde-format
msgid "&Search"
msgstr "&Hledat"

#: discover/qml/DiscoverWindow.qml:74
#, kde-format
msgid "&Installed"
msgstr "Na&instalováno"

#: discover/qml/DiscoverWindow.qml:85
#, kde-format
msgid "Fetching &updates…"
msgstr "Získávají se akt&ualizace…"

#: discover/qml/DiscoverWindow.qml:85
#, kde-format
msgid "&Update (%1)"
msgid_plural "&Updates (%1)"
msgstr[0] "Akt&ualizace (%1)"
msgstr[1] "Akt&ualizace (%1)"
msgstr[2] "Akt&ualizace (%1)"

#: discover/qml/DiscoverWindow.qml:93
#, kde-format
msgid "&About"
msgstr "Informace o &aplikaci"

#: discover/qml/DiscoverWindow.qml:101
#, kde-format
msgid "S&ettings"
msgstr "Nastav&ení"

#: discover/qml/DiscoverWindow.qml:154 discover/qml/DiscoverWindow.qml:343
#: discover/qml/DiscoverWindow.qml:450
#, kde-format
msgid "Error"
msgstr "Chyba"

#: discover/qml/DiscoverWindow.qml:158
#, kde-format
msgid "Unable to find resource: %1"
msgstr "Nelze najít zdroj: %1"

#: discover/qml/DiscoverWindow.qml:261 discover/qml/SourcesPage.qml:181
#, kde-format
msgid "Proceed"
msgstr "Pokračovat"

#: discover/qml/DiscoverWindow.qml:319
#, kde-format
msgid "Report this issue"
msgstr "Nahlásit tuto chybu"

#: discover/qml/DiscoverWindow.qml:343
#, kde-format
msgid "Error %1 of %2"
msgstr "Chyba %1 z %2"

#: discover/qml/DiscoverWindow.qml:388
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr "Zobrazit předchozí"

#: discover/qml/DiscoverWindow.qml:401
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr "Zobrazit následující"

#: discover/qml/DiscoverWindow.qml:417
#, kde-format
msgid "Copy to Clipboard"
msgstr "Zkopírovat do schránky"

#: discover/qml/Feedback.qml:13
#, kde-format
msgid "Submit usage information"
msgstr "Odeslat informace o použití"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Submitting usage information…"
msgstr "Odesílám informace o použití…"

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Configure"
msgstr "Nastavit"

#: discover/qml/Feedback.qml:22
#, kde-format
msgid "Configure feedback…"
msgstr "Nastavit zpětnou vazbu…"

#: discover/qml/Feedback.qml:29 discover/qml/SourcesPage.qml:22
#, kde-format
msgid "Configure Updates…"
msgstr "Nastavit aktualizace..."

#: discover/qml/Feedback.qml:57
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""

#: discover/qml/Feedback.qml:57
#, kde-format
msgid "Contribute…"
msgstr "Přispívejte…"

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "We are looking for your feedback!"
msgstr ""

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "Participate…"
msgstr "Přidejte se…"

#: discover/qml/InstallApplicationButton.qml:24
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "Probíhá načítání…"

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "Instalovat z %1"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Install"
msgstr "Instalovat"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgid "Remove"
msgstr "Odstranit"

#: discover/qml/InstalledPage.qml:15
#, kde-format
msgid "Installed"
msgstr "Nainstalováno"

#: discover/qml/navigation.js:18
#, kde-format
msgid "Resources for '%1'"
msgstr "Zdroje pro '%1'"

#: discover/qml/ProgressView.qml:16
#, kde-format
msgid "Tasks (%1%)"
msgstr "Úkoly (%1%)"

#: discover/qml/ProgressView.qml:16 discover/qml/ProgressView.qml:41
#, kde-format
msgid "Tasks"
msgstr "Úkoly"

#: discover/qml/ProgressView.qml:97
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3, %4  zbývá"

#: discover/qml/ProgressView.qml:98
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:99
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:62
#, kde-format
msgid "unknown reviewer"
msgstr "neznámý kontrolor"

#: discover/qml/ReviewDelegate.qml:63
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> od %2"

#: discover/qml/ReviewDelegate.qml:63
#, kde-format
msgid "Comment by %1"
msgstr "Komentář od %1"

#: discover/qml/ReviewDelegate.qml:81
#, kde-format
msgid "Version: %1"
msgstr "Verze: %1"

#: discover/qml/ReviewDelegate.qml:81
#, kde-format
msgid "Version: unknown"
msgstr "Verze: neznámá"

#: discover/qml/ReviewDelegate.qml:96
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "Hlasy: %1 z %2"

#: discover/qml/ReviewDelegate.qml:103
#, kde-format
msgid "Was this review useful?"
msgstr ""

#: discover/qml/ReviewDelegate.qml:115
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Ano"

#: discover/qml/ReviewDelegate.qml:132
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "Ne"

#: discover/qml/ReviewDialog.qml:19
#, kde-format
msgid "Reviewing %1"
msgstr "Stahuji %1"

#: discover/qml/ReviewDialog.qml:26
#, kde-format
msgid "Submit review"
msgstr "Odeslat recenzi"

#: discover/qml/ReviewDialog.qml:39
#, kde-format
msgid "Rating:"
msgstr "Hodnocení:"

#: discover/qml/ReviewDialog.qml:44
#, kde-format
msgid "Name:"
msgstr "Název:"

#: discover/qml/ReviewDialog.qml:52
#, kde-format
msgid "Title:"
msgstr "Název:"

#: discover/qml/ReviewDialog.qml:71
#, kde-format
msgid "Enter a rating"
msgstr "Zadejte hodnocení"

#: discover/qml/ReviewDialog.qml:74
#, kde-format
msgid "Write the title"
msgstr "Napište titulek"

#: discover/qml/ReviewDialog.qml:77
#, kde-format
msgid "Write the review"
msgstr "Napište hodnocení"

#: discover/qml/ReviewDialog.qml:80
#, kde-format
msgid "Keep writing…"
msgstr "Pokračujte ve psaní…"

#: discover/qml/ReviewDialog.qml:83
#, kde-format
msgid "Too long!"
msgstr "Příliš dlouhé"

#: discover/qml/ReviewDialog.qml:86
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr "Vložte název"

#: discover/qml/ReviewsPage.qml:53
#, kde-format
msgid "Reviews for %1"
msgstr "Hodnocení pro %1"

#: discover/qml/ReviewsPage.qml:61
#, kde-format
msgid "Write a Review…"
msgstr "Napište hodnocení...…"

#: discover/qml/ReviewsPage.qml:73
#, kde-format
msgid "Install this app to write a review"
msgstr ""

#: discover/qml/ReviewsPage.qml:102
#, kde-format
msgctxt "@label:listbox Most relevant reviews"
msgid "Most Relevant"
msgstr "Nejrelevantnější"

#: discover/qml/ReviewsPage.qml:109
#, kde-format
msgctxt "@label:listbox Most recent reviews"
msgid "Most Recent"
msgstr "Nejnovější"

#: discover/qml/ReviewsPage.qml:116
#, kde-format
msgctxt "@label:listbox Reviews with the highest ratings"
msgid "Highest Ratings"
msgstr "Nejvyšší hodnocení"

#: discover/qml/ReviewsStats.qml:51
#, kde-format
msgctxt "how many reviews"
msgid "%1 reviews"
msgstr "%1 recenzí"

#: discover/qml/ReviewsStats.qml:69
#, kde-format
msgctxt "widest character in the language"
msgid "M"
msgstr "M"

#: discover/qml/ReviewsStats.qml:143
#, kde-format
msgid "Unknown reviewer"
msgstr "Neznámý kontrolor"

#: discover/qml/ReviewsStats.qml:163
#, kde-format
msgctxt "Opening upper air quote"
msgid "“"
msgstr "“"

#: discover/qml/ReviewsStats.qml:178
#, kde-format
msgctxt "Closing lower air quote"
msgid "„"
msgstr "„"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search…"
msgstr "Hledat…"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search in '%1'…"
msgstr "Hledat v '%1'…"

#: discover/qml/SourcesPage.qml:18
#, kde-format
msgid "Settings"
msgstr "Nastavení"

#: discover/qml/SourcesPage.qml:99
#, kde-format
msgid "Default source"
msgstr "Výchozí zdroj"

#: discover/qml/SourcesPage.qml:106
#, kde-format
msgid "Add Source…"
msgstr "Přidat zdroj…"

#: discover/qml/SourcesPage.qml:132
#, kde-format
msgid "Make default"
msgstr "Nastavit jako výchozí"

#: discover/qml/SourcesPage.qml:223
#, kde-format
msgid "Increase priority"
msgstr "Zvýšit prioritu"

#: discover/qml/SourcesPage.qml:229
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "Zvýšení předvolby '%1' selhalo"

#: discover/qml/SourcesPage.qml:235
#, kde-format
msgid "Decrease priority"
msgstr "Snížit prioritu"

#: discover/qml/SourcesPage.qml:241
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "Snížení předvolby '%1' selhalo"

#: discover/qml/SourcesPage.qml:247
#, kde-format
msgid "Remove repository"
msgstr "Odstranit repozitář"

#: discover/qml/SourcesPage.qml:258
#, kde-format
msgid "Show contents"
msgstr "Zobrazit obsah"

#: discover/qml/SourcesPage.qml:298
#, kde-format
msgid "Missing Backends"
msgstr "Chybějící podpůrné vrstvy"

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "Aktualizace"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Update Issue"
msgstr "Problém aktualizace"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr "Technické podrobnosti"

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr "Zobrazit technické podrobnosti"

#: discover/qml/UpdatesPage.qml:94
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid ""
"If the error indicated above looks like a real issue and not a temporary "
"network error, please report it to %1, not KDE."
msgstr ""

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Copy Text"
msgstr "Kopírovat text"

#: discover/qml/UpdatesPage.qml:106
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid "Error message copied. Remember to report it to %1, not KDE!"
msgstr ""

#: discover/qml/UpdatesPage.qml:113
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro/OS"
msgid "Report Issue to %1"
msgstr "Nahlaste problém na %1"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update the selected items' "
msgid "Update Selected"
msgstr "Aktualizovat vybrané"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "Aktualizovat vše"

#: discover/qml/UpdatesPage.qml:181
#, kde-format
msgid "Ignore"
msgstr "Ignorovat"

#: discover/qml/UpdatesPage.qml:227
#, kde-format
msgid "Select All"
msgstr "Vybrat vše"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Select None"
msgstr "Zrušit výběr"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "Automaticky restartovat po dokončení aktualizace"

#: discover/qml/UpdatesPage.qml:249
#, kde-format
msgid "Total size: %1"
msgstr "Celková velikost: %1"

#: discover/qml/UpdatesPage.qml:284
#, kde-format
msgid "Restart Now"
msgstr "Restartovat nyní"

#: discover/qml/UpdatesPage.qml:387
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:403
#, kde-format
msgid "Installing"
msgstr "Instaluje se"

#: discover/qml/UpdatesPage.qml:439
#, kde-format
msgid "Update from:"
msgstr "Aktualizace z:"

#: discover/qml/UpdatesPage.qml:451
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:458
#, kde-format
msgid "More Information…"
msgstr "Více informací…"

#: discover/qml/UpdatesPage.qml:486
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "Získávají se aktualizace…"

#: discover/qml/UpdatesPage.qml:499
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Aktualizace"

#: discover/qml/UpdatesPage.qml:508
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "Pro dokončení aktualizace restartujte systém"

#: discover/qml/UpdatesPage.qml:520 discover/qml/UpdatesPage.qml:527
#: discover/qml/UpdatesPage.qml:534
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "Aktuální"

#: discover/qml/UpdatesPage.qml:541
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Měli byste zkontrolovat aktualizace"

#: discover/qml/UpdatesPage.qml:548
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr ""
